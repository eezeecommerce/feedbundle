<?php

namespace eezeecommerce\FeedBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/feed")
     * @Template("@eezeecommerceFeed/Default/index.rss.twig")
     */
    public function indexAction()
    {
        $products = $this->getDoctrine()->getRepository("eezeecommerceProductBundle:Product")
            ->findAllDisabledArray();
        return array("products" => $products);
    }
}
